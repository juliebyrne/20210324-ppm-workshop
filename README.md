# 2021-03-24 PPM Workshop

Please download the user guide in this repo for the GitLab PPM Workshop on March 24, 2021.  Click the link to access the guide: [03-24-21 PPM Student Guide.pdf](https://gitlab.com/juliebyrne/20210324-ppm-workshop/-/blob/master/03-24-2021%20PPM%20Student%20Guide.pdf)

 then use the download button to download.
 ![alt text](download.png)
 



